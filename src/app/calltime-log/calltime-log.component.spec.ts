import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CalltimeLogComponent } from './calltime-log.component';

describe('CalltimeLogComponent', () => {
  let component: CalltimeLogComponent;
  let fixture: ComponentFixture<CalltimeLogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CalltimeLogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CalltimeLogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
