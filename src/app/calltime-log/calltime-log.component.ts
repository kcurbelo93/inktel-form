import { Component, OnInit, EventEmitter, Input, Output, ElementRef, AfterViewInit } from '@angular/core';

@Component({
  selector: 'app-calltime-log',
  // templateUrl: './calltime-log.component.html',
  template:`
  <div class="d-block px-1 ">
  Start: <timepicker [(ngModel)]="startTime" [showMeridian]=false [showSpinners]=false (isValid)="isValid($event)"></timepicker>
  </div>
  <div class="d-block px-1 font-sm">
  End: <timepicker [(ngModel)]="endTime" [showMeridian]=false [showSpinners]=false (isValid)="isValid($event)" ></timepicker>
  </div>
  <div class="d-block pt-2" *ngIf="showFileUploader">
    <span class="font-weight-light" > AH Override </span>  <br>
    <input type="file" (change)="onFileChange($event)">
  </div>
  `
})
export class CalltimeLogComponent implements OnInit {
  //extra inputs/parameters are used so the component can be customized globally
  @Input('start')  defaultStartTime: any;
  @Input('stop')  defaultEndTime: any;
  @Input()  size: string;
  @Input()  meridian: boolean;
  @Input()  spinners: boolean;
  @Input()  uploader: boolean;
  @Output() callLog = new EventEmitter();

  isMeridian: boolean = this.meridian ? this.meridian: true;
  showSpinners: boolean = this.spinners ? this.spinners : false;
  pickerSize = this.size ? this.size : 'small';
  showFileUploader: boolean = !this.uploader ? !this.uploader : true;

  startTime:any;
  endTime:any; 
  file:any;

  constructor(private elementRef:ElementRef) {}
  
  ngOnInit() {
    //initialize values and assign a default is none are passed/given
    this.startTime = this.defaultStartTime ?  new Date(this.defaultStartTime): new Date()//this.startTime;
    this.endTime = this.defaultEndTime ?  new Date(this.defaultEndTime): new Date()//this.startTime;
  }
  //access files being uploaded. Sends response if file exists.
  //Needs additional development to customize file type acceptance and validation.
  private onFileChange(event) {
    console.log(event)
    if (event.target.files.length > 0) {
      this.file = event.target.files[0];
      this.emitCallLog();
    }
  }
  //validates id values for timepicker are valid
  //if valid send response.
  //sends immediately after time is updated.
  isValid(valid){
    // console.log('isvalid event', valid);
    if(valid){
      this.emitCallLog()
    }  
  }
// function used to send response
  emitCallLog(){
    this.callLog.emit({
      startTime: new Date(this.startTime).toISOString(),
      endTime: new Date(this.endTime).toISOString(),
      AfterHoursOverride: this.file
    });
  }

}
