import { Component, OnInit, EventEmitter, Input, Output } from '@angular/core';
import { faQuestionCircle } from '@fortawesome/free-solid-svg-icons'

@Component({
  selector: 'app-daily-log',
  templateUrl: './daily-log.component.html',
  styleUrls: ['./daily-log.component.scss']
})
export class DailyLogComponent implements OnInit {
  @Input('defaultStart')  defaultStartTime: any;
  @Input('defaultStop')  defaultEndTime: any;
  @Input() monStart: any;
  @Input() monStop: any;
  @Input() tueStart: any;
  @Input() tueStop: any;
  @Input() wedStart: any;
  @Input() wedStop: any;
  @Input() thuStart: any;
  @Input() thuStop: any;
  @Input() friStart: any;
  @Input() friStop: any;
  @Input() satStart: any;
  @Input() satStop: any;
  @Input() sunStart: any;
  @Input() sunStop: any;

  @Output() weekLog = new EventEmitter();
  
  defaultStart:any;
  defaultEnd:any;
  mondayStart: any;
  mondayStop: any;
  tuesdayStart: any;
  tuesdayStop: any;
  wednesdayStart: any;
  wednesdayStop: any;
  thursdayStart: any;
  thursdayStop: any;
  fridayStart: any;
  fridayStop: any;
  saturdayStart: any;
  saturdayStop: any;
  sundayStart: any;
  sundayStop: any;

  callLogHelper = 'Enter Call Log for the week.\n Use the AH Override to load file record.'
//Icons
  faQuestionCircle = faQuestionCircle;

  constructor() { }
  //initialize variables and allows user to pass variables for each day. Else, the day would resort to default values
  ngOnInit() {
  this.mondayStart   = this.monStart ? this.monStart : this.defaultStartTime;
  this.mondayStop    = this.monStop ? this.monStop : this.defaultEndTime;
  this.tuesdayStart  = this.tueStart ? this.tueStart : this.defaultStartTime;
  this.tuesdayStop   = this.tueStop ? this.tueStop : this.defaultEndTime;
  this.wednesdayStart= this.wedStart ? this.wedStart : this.defaultStartTime;
  this.wednesdayStop = this.wedStop ? this.wedStop : this.defaultEndTime;
  this.thursdayStart = this.thuStart ? this.thuStart : this.defaultStartTime;
  this.thursdayStop  = this.thuStop ? this.thuStop : this.defaultEndTime;
  this.fridayStart   = this.friStart ? this.friStart : this.defaultStartTime;
  this.fridayStop    = this.friStop ? this.friStop : this.defaultEndTime;
  this.saturdayStart = this.satStart ? this.satStart : this.defaultStartTime;
  this.saturdayStop  = this.satStop ? this.satStop : this.defaultEndTime;
  this.sundayStart   = this.sunStart ? this.sunStart : this.defaultStartTime;
  this.sundayStop    = this.satStart ? this.satStart : this.defaultEndTime;
  }

  //FUNCTION used to send response and create object
  passInfo(event, day){
    // console.log('pass info', event)
    let obj = {}
    obj[day] = event
    this.weekLog.emit(obj)
  }
}
