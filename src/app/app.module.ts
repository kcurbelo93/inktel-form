import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule , FormsModule} from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MyformComponent } from './myform/myform.component';

// import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { CalltimeLogComponent } from './calltime-log/calltime-log.component';
import { DailyLogComponent } from './daily-log/daily-log.component';

import { TimepickerModule, TooltipModule, BsDropdownModule } from 'ngx-bootstrap';

import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';



@NgModule({
  declarations: [
    AppComponent,
    MyformComponent,
    CalltimeLogComponent,
    DailyLogComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    FormsModule,
    // NgbModule,
    TimepickerModule.forRoot(), BsDropdownModule.forRoot(), TooltipModule.forRoot(),
    FontAwesomeModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
