import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl , FormBuilder,  Validators } from '@angular/forms';
import { faQuestionCircle } from '@fortawesome/free-solid-svg-icons'

@Component({
  selector: 'app-myform',
  templateUrl: './myform.component.html',
  styleUrls: ['./myform.component.scss']
})
export class MyformComponent implements OnInit {

  callTimes: FormGroup;
  meridian = true;
  currCallLog: any = {};
  campaingList: any = [];
  inboudGroupsList: any = [];
  statesRules = [
    {id: 1, state: 'Alabama', start: '8:00a', end: '8:00p'},
    {id: 2, state: 'Georgia', start: '8:00a', end: '8:00p'},
    {id: 3, state: 'Florida', start: '8:00a', end: '8:00p'},
    {id: 4, state: 'Texas', start: '8:00a', end: '8:00p'}
  ];
  monday = new Date();
  // Icons
  faQuestionCircle = faQuestionCircle;

  constructor( public form: FormBuilder) { 
    this.callTimes = new FormGroup({
      'id': new FormControl('24hour', [Validators.required, Validators.minLength(2)] ),
      'name': new FormControl('Keith', [Validators.required, Validators.minLength(2)]),
      'comments': new FormControl(''),
      'defaultStart': new FormControl(new Date(1776, 6, 4, 5, 30, 0, 0)),
      'defaultEnd': new FormControl(new Date(1776, 6, 4, 13, 30, 0, 0)),
      'stateRule': new FormControl('null'),
      'callLog': new FormControl(this.currCallLog),
      'campaings': new FormControl('null'),
      'inboudGroups': new FormControl('null'),
    });

  }

  ngOnInit() {
    console.log(this.callTimes)
  }

  // toggleMeridian() {
  //   this.meridian = !this.meridian;
  // }
  
  submit(){
    console.log(this.callTimes)
    // console.log(this.currCallLog)
  }

  showCallLog(event){
    // console.log('show call log',event)
    this.currCallLog["Monday"]= event && event.Monday ? event.Monday : this.currCallLog["Monday"];
    this.currCallLog["Tuesday"]= event && event.Tuesday ? event.Tuesday : this.currCallLog["Tuesday"];
    this.currCallLog["Wednesday"]= event && event.Wednesday ? event.Wednesday : this.currCallLog["Wednesday"];
    this.currCallLog["Thursday"]= event && event.Thursday ? event.Thursday : this.currCallLog["Thursday"];
    this.currCallLog["Friday"]= event && event.Friday ? event.Friday : this.currCallLog["Friday"];
    this.currCallLog["Saturday"]= event && event.Saturday ? event.Saturday : this.currCallLog["Saturday"];
    this.currCallLog["Sunday"]= event && event.Sunday ? event.Sunday : this.currCallLog["Sunday"];
  }

  onChange(val){
    // console.log(val.target.value)
    this.callTimes.controls['stateRule'].setValue(val.target.value)
  }
}
/* 
Git Commit Message:

The previous form needed to be redesigned and accepted the json at bottom of page (commented out).

Changes Made:
-Removed unnecessary questions mark that served as information/help guides. 
-Added tooltips where needed.(Mostly titles to explain purpose of section)
- Using ngx-bootsrap and bootstrap for css/sass library. 
(npm install ngx-bootstrap bootstrap jquery popper.js --save)
- Using fontawesome for angular for icons.
(npm i @fortawesome/fontawesome-svg-core @fortawesome/free-solid-svg-icons @fortawesome/angular-fontawesome -save)
- Created a component for the daily call log (named daily-log).
- Daily-log component is created from multiple call log inputs (see calltime-log component)
  for each day of week.
- Attempted to group most important fields together.
- Returns a custom JSON object.
- Single submit button. (does not allow submit if form is invalid)

What needs work/attention:
- The file upload presentation needs validators and addition css to correct display. 
  possibly build new input or use 3rd party. (Needs Discussion).
- Needs more form validators. Wasn't sure which field where mandatory.
- All tooltips need to be updated to display correct text/explaintation.
- Timepicker for setting default values is in meridian(Am/Pm) and weekly values are set in military(24hr)
  need to make them uniform. (Needs discussion)
- Timepicker accepts only accepts values in "new Date()"" format.
- Need to assign values to campaing and inbound usage list to populate template.
- Need to implement functionality to update "Active State Call Time Definitions". 
- State Rules dropdown list needs to be populated from database. Currently using a small custom array of objects. 


*/



// {
//   "call_times": [{

//           "call_time_id": "24hours",

//           "call_time_name": "default 24 hours calling",

//           "call_time_comments": "",

//           "ct_default_start": 0,

//           "ct_default_stop": 2400,

//           "ct_sunday_start": 0,

//           "ct_sunday_stop": 2400,

//           "ct_monday_start": 0,

//           "ct_monday_stop": 2400,

//           "ct_tuesday_start": 0,

//           "ct_tuesday_stop": 2400,

//           "ct_wednesday_start": 0,

//           "ct_wednesday_stop": 2400,

//           "ct_thursday_start": 0,

//           "ct_thursday_stop": 2400,

//           "ct_friday_start": 0,

//           "ct_friday_stop": 2400,

//           "ct_saturday_start": 0,

//           "ct_saturday_stop": 2400,

//           "ct_state_call_times": null,

//           "default_afterhours_filename_override": "",

//           "sunday_afterhours_filename_override": "",

//           "monday_afterhours_filename_override": "",

//           "tuesday_afterhours_filename_override": "",

//           "wednesday_afterhours_filename_override": "",

//           "thursday_afterhours_filename_override": "",

//           "friday_afterhours_filename_override": "",

//           "saturday_afterhours_filename_override": ""

//       }

// ]

// }

